package model;

public class User {

    private int id;
    private String full_name;
    private String billing_address;
    private String login;
    private String password;

    public User(int id, String full_name, String billing_address, String login, String password) {
        this.id = id;
        this.full_name = full_name;
        this.billing_address = billing_address;
        this.login = login;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getFull_name() {
        return full_name;
    }

    public String getBilling_address() {
        return billing_address;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", full_name='" + full_name + '\'' +
                ", billing_address='" + billing_address + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

}
