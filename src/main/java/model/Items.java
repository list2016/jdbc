package model;

import java.time.LocalDate;

public class Items {

    private int item_id;
    private String title;
    private String description;
    private double start_price;
    private double bid_increment;
    private LocalDate start_date;
    private LocalDate stop_date;
    private boolean by_it_now;
    private int users_user_id;

    public Items(int item_id, String title, String description, double start_price, double bid_increment, LocalDate start_date, LocalDate stop_date, boolean by_it_now, int users_user_id) {
        this.item_id = item_id;
        this.title = title;
        this.description = description;
        this.start_price = start_price;
        this.bid_increment = bid_increment;
        this.start_date = start_date;
        this.stop_date = stop_date;
        this.by_it_now = by_it_now;
        this.users_user_id = users_user_id;
    }

    public int getItem_id() {
        return item_id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public double getStart_price() {
        return start_price;
    }

    public double getBid_increment() {
        return bid_increment;
    }

    public LocalDate getStart_date() {
        return start_date;
    }

    public LocalDate getStop_date() {
        return stop_date;
    }

    public boolean isBy_it_now() {
        return by_it_now;
    }

    public int getUsers_user_id() {
        return users_user_id;
    }

    @Override
    public String toString() {
        return "Items{" +
                "item_id=" + item_id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", start_price=" + start_price +
                ", bid_increment=" + bid_increment +
                ", start_date=" + start_date +
                ", stop_date=" + stop_date +
                ", by_it_now=" + by_it_now +
                ", users_user_id=" + users_user_id +
                '}';
    }
}
