package model;

import java.time.LocalDate;

public class Bids {
    private int bid_id;
    private LocalDate bid_date;
    private double bid_value;
    private int items_item_id;
    private int users_user_id;

    public Bids(int bid_id, LocalDate bid_date, double bid_value, int items_item_id, int users_user_id) {
        this.bid_id = bid_id;
        this.bid_date = bid_date;
        this.bid_value = bid_value;
        this.items_item_id = items_item_id;
        this.users_user_id = users_user_id;
    }

    public int getBid_id() {
        return bid_id;
    }

    public LocalDate getBid_date() {
        return bid_date;
    }

    public double getBid_value() {
        return bid_value;
    }

    public int getItems_item_id() {
        return items_item_id;
    }

    public int getUsers_user_id() {
        return users_user_id;
    }

    @Override
    public String toString() {
        return "Bids{" +
                "bid_id=" + bid_id +
                ", bid_date=" + bid_date +
                ", bid_value=" + bid_value +
                ", items_item_id=" + items_item_id +
                ", users_user_id=" + users_user_id +
                '}';
    }
}
