package service;

import dao.BidsDao;
import dao.ItemsDao;
import dao.UserDao;
import model.Items;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class DemoService implements IService {

    @Autowired
    UserDao userDao;
    @Autowired
    BidsDao bidsDao;
    @Autowired
    ItemsDao itemsDao;

    public void execute(){
        System.out.println("Список ставок данного пользователя:");
        bidsDao.getBidsByUsers(2).forEach(System.out::println);

        System.out.println("Список лотов данного пользователя:");
        itemsDao.getListItemsByUsers(2).forEach(System.out::println);

        System.out.println("Поиск лота по строке в названии:");
        itemsDao.getListItemsByName("ваза").forEach(System.out::println);

        System.out.println("Поиск лота по строке в описании:");
        itemsDao.getListItemsByDescription("обычная ваза").forEach(System.out::println);

        System.out.println("Средняя цена лотов каждого пользователя:");
        itemsDao.getMiddlePriceByUsers().forEach(System.out::println);

        System.out.println("Максимальный размер ставок на каждый лот:");
        bidsDao.getMaxBidsForEveryItems().forEach(System.out::println);

        System.out.println("Список дейстующих лотов данного пользователя");
        bidsDao.getActiveItemsByUsers(2).forEach(System.out::println);

        System.out.println("Добавление нового пользователя");
        userDao.addUsers(new User(3,"Саша","Москва","qwert","qwert123"));

        System.out.println("Добавление нового лота");
        itemsDao.addItems(new Items(4,"картина","Малевич",100000, 100000,
                LocalDate.of(2019,1,8),
                LocalDate.of(2019,1,9),
                false,2));

        System.out.println("Удаление ставок данного пользователя");
        bidsDao.deleteBidsByUsers(2);

        System.out.println("Удаление лотов данного пользователя");
        itemsDao.deleteItems(2);

        System.out.println("Удвоить стартовые цены товров данного пользователя");
        itemsDao.doubleUpStartPrice(1);

        System.out.println("Удвоение стартовых цен для всех пользователей");
        itemsDao.doubleUpStartPriceForAll();

    }
   }