package dao;

import model.Items;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Map;

@Repository

public class ItemsDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private RowMapper<Items> ROW_MAPPER = (resultSet, rowNumbers) -> new Items(
            resultSet.getInt("item_id"),
            resultSet.getString("title"),
            resultSet.getString("description"),
            resultSet.getDouble("start_price"),
            resultSet.getDouble("bid_increment"),
            resultSet.getDate("start_date").toLocalDate(),
            resultSet.getDate("stop_date").toLocalDate(),
            resultSet.getBoolean("by_it_now"),
            resultSet.getInt("users_user_id")
    );

    public List<Items> getListItemsByUsers(int user_id) {
        String sql = "SELECT * FROM db2.items where users_user_id = ? ;";
        return jdbcTemplate.query(sql, new Object[]{user_id}, ROW_MAPPER);
    }

    public List<Items> getListItemsByName(String title) {
        title = "%" + title + "%";
        String sql = "SELECT * FROM db2.items where title = ? ;";
        return jdbcTemplate.query(sql, new Object[]{title}, ROW_MAPPER);
    }

    public List<Items> getListItemsByDescription(String description) {
        description = "%" + description + "%";
        String sql = "SELECT * FROM db2.items where description = ? ;";
        return jdbcTemplate.query(sql, new Object[]{description}, ROW_MAPPER);
    }

    public List<Map<String, Object>> getMiddlePriceByUsers() {
        String sql = "SELECT full_name, AVG(start_price) " +
                "FROM db2.items INNER JOIN db2.users ON items.users_user_id = users.users_id " +
                "GROUP BY users_user_id;";
        return jdbcTemplate.queryForList(sql);
    }

    public void doubleUpStartPriceForAll() {
        String sql = "update db2.items set start_price = start_price * 2 where users_user_id > 0;";
        jdbcTemplate.update(sql);
    }

    public void doubleUpStartPrice(int user_id) {
        String sql = "update db2.items set start_price = start_price * 2 where users_user_id = ?;";
        jdbcTemplate.update(sql, new Object[]{user_id});
    }

    public void addItems(Items item) {
        String sql = "insert into db2.items " +
                "(item_id, title, description, start_price, bid_increment, start_date, stop_date, users_user_id) " +
                "values (?,?,?,?,?,?,?,?); ";
        jdbcTemplate.update(sql,
                item.getItem_id(),
                item.getTitle(),
                item.getDescription(),
                item.getStart_price(),
                item.getBid_increment(),
                Date.valueOf(item.getStart_date()),
                Date.valueOf(item.getStop_date()),
                item.getUsers_user_id());
    }

    public void deleteItems(int user_id) {
        String sql = "delete from db2.items where users_user_id = ?;";
        jdbcTemplate.update(sql, new Object[]{user_id});
    }




}
