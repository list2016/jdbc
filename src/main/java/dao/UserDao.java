package dao;

import model.User;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

@Repository

public class UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<User> ROW_MAPPER = (resultSet, rowNumbers) -> new User(
            resultSet.getInt("users_id"),
            resultSet.getString("full_name"),
            resultSet.getString("billing_address"),
            resultSet.getString("login"),
            resultSet.getString("password"));

    public void addUsers(User user) {
        String sql = "insert into db2.users (users_id, full_name, billing_address, login, password)" +
                " values (?,?,?,?,?); ";
        jdbcTemplate.update(sql,
                user.getId(),
                user.getFull_name(),
                user.getBilling_address(),
                user.getLogin(),
                user.getPassword());
    }

}
