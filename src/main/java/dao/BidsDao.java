package dao;

import model.Bids;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public class BidsDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private RowMapper<Bids> ROW_MAPPER = (resultSet, rowNumbers) -> new Bids(
            resultSet.getInt("Bid_id"),
            resultSet.getDate("bid_date").toLocalDate(),
            resultSet.getDouble("bid_value"),
            resultSet.getInt("items_item_id"),
            resultSet.getInt("users_user_id"));

    public List<Bids> getBidsByUsers(int user_id) {
        String sql = "SELECT * FROM db2.bids where users_user_id = ?;";
        return jdbcTemplate.query(sql,new Object[]{user_id}, ROW_MAPPER);
    }

    public List<Bids> getMaxBidsForEveryItems() {
        String sql = "SELECT items_item_id, max(bid_value) FROM db2.bids where items_item_id != 0 group by items_item_id; ";
        return jdbcTemplate.query(sql, ROW_MAPPER);
    }

    public void deleteBidsByUsers(int user_id) {
        String sql = "delete from db2.bids where users_user_id = ?;";
        jdbcTemplate.update(sql,new Object[]{user_id});
    }

    public List<Bids> getActiveItemsByUsers(int user_id) {
        String sql = "SELECT * FROM db2.bids where bid_date > now() and users_user_id = ?;";
        return jdbcTemplate.query(sql,new Object[]{user_id}, ROW_MAPPER);
    }

}
